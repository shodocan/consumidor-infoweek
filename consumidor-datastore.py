import time
from google.cloud import pubsub_v1
from google.cloud import datastore
import json
import logging


class Consumidor:
    def __init__(self,project):
        self.project = project
        self.client = datastore.Client(self.project)

    def receive_messages(self, subscription_name):
        subscriber = pubsub_v1.SubscriberClient()
        subscription_path = "projects/{PID}/subscriptions/{SUB}".format(PID=self.project,SUB=subscription_name)
        subscriber.subscribe(subscription_path, callback=self.salvaDatastore)        
    
    def salvaDatastore(self, message):
        try:
            aluno = json.loads(message.data)
            aluno_key = self.client.key('Aluno',aluno['id'])
            aluno_datastore = datastore.Entity(key=aluno_key)
            aluno_datastore.update(aluno)
            self.client.put(aluno_datastore)
            message.ack()
            print('Saved {}: {}'.format(aluno['id'],aluno['nome']))
        except Exception as e:
            message.ack()
            print(e.message)


def main():
    consumidor = Consumidor("infoweekexample")
    consumidor.receive_messages("salvarAlunoDatastore")
    print('Listening for messages on {}'.format("salvarAlunoDatastore"))
    while True:
        time.sleep(60)


if __name__ == "__main__":
    main()